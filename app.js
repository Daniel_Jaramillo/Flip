const PORT = 2000;

var express = require ('express');
var app = express();
var serv = require('http').Server(app);

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/client/index.html');
});
app.use('/client', express.static(__dirname + '/client'));

serv.listen(PORT);
console.log(`Running in localhost at port ${PORT}`);

var io = require('socket.io')(serv);
io.sockets.on('connection', (socket)=>{
    console.log('Socket connection');
    console.log(socket.id);
})